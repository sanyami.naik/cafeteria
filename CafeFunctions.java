package Code2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

public class CafeFunctions {

    static int capacityOfTwo = 8;
    static int capacityOfFour = 4;
    static int capacityOfSix = 2;
    static int capacityOfEight = 2;
    static int staticTableNo = 1;

    public static ArrayList<CafePojo> arrayList = new ArrayList<>();

    public static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    void bookTable() throws IOException {
        CafePojo cafePojo1 = new CafePojo();
        System.out.println("Enter your name");
        String name = bufferedReader.readLine();
        cafePojo1.setCustomerName(name);
        System.out.println("Enter the number of seats");
        int number = Integer.parseInt(bufferedReader.readLine());
        if (number > 0 && number <= 2 && capacityOfTwo > 0) {
            cafePojo1.setNoOfSeats(number);
            System.out.println("Table booked");
            cafePojo1.tableNo = staticTableNo;
            System.out.println("Your table number is " + cafePojo1.tableNo);
            arrayList.add(cafePojo1);
            staticTableNo++;
            capacityOfTwo--;
        } else if (number > 2 && number <= 4 && capacityOfFour > 0) {
            cafePojo1.setNoOfSeats(number);
            System.out.println("Table booked");
            cafePojo1.tableNo = staticTableNo;
            System.out.println("Your table number is " + cafePojo1.tableNo);
            arrayList.add(cafePojo1);
            staticTableNo++;
            capacityOfFour--;
        } else if (number > 4 && number <= 6 && capacityOfSix > 0) {
            cafePojo1.setNoOfSeats(number);
            System.out.println("Table booked");
            cafePojo1.tableNo = staticTableNo;
            System.out.println("Your table number is " + cafePojo1.tableNo);
            arrayList.add(cafePojo1);
            staticTableNo++;
            capacityOfSix--;
        } else if (number > 6 && number <= 8 && capacityOfEight > 0) {
            cafePojo1.setNoOfSeats(number);
            System.out.println("Table booked");
            cafePojo1.tableNo = staticTableNo;
            System.out.println("Your table number is " + cafePojo1.tableNo);
            arrayList.add(cafePojo1);
            staticTableNo++;
            capacityOfEight--;
        } else
            System.out.println("No Seats Availble");
    }

    void showMenu() {
        System.out.println("The menu for today is:");
        System.out.println("1. coffee");
        System.out.println("2. tea");
        System.out.println("3. juice");
        System.out.println();
        System.out.println("Place your order");

    }

    void giveOrder() throws IOException {
        CafePojo cafePojo2 = new CafePojo();
        int flag = 0;
        boolean orderOk = false;
        System.out.println("Enter the table number you want to place order for");
        int tableNo = Integer.parseInt(bufferedReader.readLine());
        for (CafePojo index : arrayList) {
            if (tableNo == index.tableNo) {
                cafePojo2 = index;
                flag = 1;
                break;
            }
        }

        try {
            if (flag == 1) {
                int n = 1;

                new CafeFunctions().showMenu();
                while (n != 0) {
                    System.out.println("Enter the serial number of item you want");
                    n = Integer.parseInt(bufferedReader.readLine());
                    if (n == 1) {
                        cafePojo2.s1 = n;
                        System.out.println("enter the number of coffees you want");
                        cafePojo2.coffeNo = Integer.parseInt(bufferedReader.readLine());
                        orderOk = true;
                    } else if (n == 2) {
                        cafePojo2.s2 = n;
                        System.out.println("enter the number of teas you want");
                        cafePojo2.teaNo = Integer.parseInt(bufferedReader.readLine());
                    } else if (n == 3) {
                        cafePojo2.s3 = n;
                        System.out.println("enter the number of juices you want");
                        cafePojo2.juiceNo = Integer.parseInt(bufferedReader.readLine());
                    }
                }

            } else
                throw new NoReservationFoundException();
        } catch (NoReservationFoundException n) {

        }

        try {
            if (orderOk == false)
                throw new InValidOrderException();
        } catch (InValidOrderException i) {
            System.out.println("You must order coffee or else your order will be cancelled ");
        }

        totalPrice(cafePojo2);
    }

    void detailsOfUsers() {
        System.out.println("The list in sorted names");
        arrayList.stream().forEach(x -> System.out.println(x));


        System.out.println("The list in descending order of total prices is");
        Collections.sort(arrayList,new SortByDescendingValues());
        arrayList.stream().forEach(x -> System.out.println(x));
    }

    void totalPrice(CafePojo c) {
        int coffeePrice=c.coffeNo*40;
        int teaPrice=c.teaNo*20;
        int juicePrice=c.juiceNo*50;

        c.totalPrice=c.totalPrice+coffeePrice+teaPrice+juicePrice;
        System.out.println("Your order cost is "+c.totalPrice);
    }
}























/*
OUTPUT:
Choose from below
0 For exit
1 For booking table
2 For displaying menu
3 For ordering
4 For seeing order list
Enter the choice
1
Enter your name
Sanymi
Enter the number of seats
2
Table booked
Your table number is 1
Choose from below
0 For exit
1 For booking table
2 For displaying menu
3 For ordering
4 For seeing order list
Enter the choice
3
Enter the table number you want to place order for
1
The menu for today is:
1. coffee
2. tea
3. juice

Place your order
Enter the serial number of item you want
1
enter the number of coffees you want
2
Enter the serial number of item you want
0
Your order cost is 80
Choose from below
0 For exit
1 For booking table
2 For displaying menu
3 For ordering
4 For seeing order list
Enter the choice
1
Enter your name
Aman
Enter the number of seats
6
Table booked
Your table number is 2
Choose from below
0 For exit
1 For booking table
2 For displaying menu
3 For ordering
4 For seeing order list
Enter the choice
3
Enter the table number you want to place order for
2
The menu for today is:
1. coffee
2. tea
3. juice

Place your order
Enter the serial number of item you want
1
enter the number of coffees you want
4
Enter the serial number of item you want
2
enter the number of teas you want
1
Enter the serial number of item you want
0
Your order cost is 180
Choose from below
0 For exit
1 For booking table
2 For displaying menu
3 For ordering
4 For seeing order list
Enter the choice
1
Enter your name
Lasyaa
Enter the number of seats
6
Table booked
Your table number is 3
Choose from below
0 For exit
1 For booking table
2 For displaying menu
3 For ordering
4 For seeing order list
Enter the choice
3
Enter the table number you want to place order for
3
The menu for today is:
1. coffee
2. tea
3. juice

Place your order
Enter the serial number of item you want
1
enter the number of coffees you want
1
Enter the serial number of item you want
3
enter the number of juices you want
5
Enter the serial number of item you want
0
Your order cost is 290
Choose from below
0 For exit
1 For booking table
2 For displaying menu
3 For ordering
4 For seeing order list
Enter the choice
1
Enter your name
Om
Enter the number of seats
6
No Seats Availble
Choose from below
0 For exit
1 For booking table
2 For displaying menu
3 For ordering
4 For seeing order list
Enter the choice
4
The list in sorted names
CafePojo{customerName='Sanymi', noOfSeats=2, tableNo=1, s1=1, s2=0, s3=0, coffeNo=2, teaNo=0, juiceNo=0, totalPrice=80}
CafePojo{customerName='Aman', noOfSeats=6, tableNo=2, s1=1, s2=2, s3=0, coffeNo=4, teaNo=1, juiceNo=0, totalPrice=180}
CafePojo{customerName='Lasyaa', noOfSeats=6, tableNo=3, s1=1, s2=0, s3=3, coffeNo=1, teaNo=0, juiceNo=5, totalPrice=290}
The list in descending order of total prices is
CafePojo{customerName='Lasyaa', noOfSeats=6, tableNo=3, s1=1, s2=0, s3=3, coffeNo=1, teaNo=0, juiceNo=5, totalPrice=290}
CafePojo{customerName='Aman', noOfSeats=6, tableNo=2, s1=1, s2=2, s3=0, coffeNo=4, teaNo=1, juiceNo=0, totalPrice=180}
CafePojo{customerName='Sanymi', noOfSeats=2, tableNo=1, s1=1, s2=0, s3=0, coffeNo=2, teaNo=0, juiceNo=0, totalPrice=80}
Enter a valid choice
Choose from below
0 For exit
1 For booking table
2 For displaying menu
3 For ordering
4 For seeing order list
Enter the choice
1
Enter your name
Eva
Enter the number of seats
2
Table booked
Your table number is 4
Choose from below
0 For exit
1 For booking table
2 For displaying menu
3 For ordering
4 For seeing order list
Enter the choice
3
Enter the table number you want to place order for
4
The menu for today is:
1. coffee
2. tea
3. juice

Place your order
Enter the serial number of item you want
2
enter the number of teas you want
3
Enter the serial number of item you want
0
Atleast one coffee must be ordered while placing an order
You must order coffee or else your order will be cancelled
Your order cost is 60
Choose from below
0 For exit
1 For booking table
2 For displaying menu
3 For ordering
4 For seeing order list
Enter the choice

 */
