package Code2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class CafeMenu {
    public static BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
    public static void main(String[] args) throws IOException {

        CafeFunctions cafeFunctions=new CafeFunctions();
        while(true) {

            System.out.println("Choose from below");
            System.out.println("0 For exit");
            System.out.println("1 For booking table");
            System.out.println("2 For displaying menu");
            System.out.println("3 For ordering");
            System.out.println("4 For seeing order list");
            System.out.println("Enter the choice");
            int choice=Integer.parseInt(bufferedReader.readLine());
                switch (choice)
                {
                    case 0:
                        System.exit(0);
                        break;

                    case 1:
                         cafeFunctions.bookTable();
                        break;

                    case 2:
                        cafeFunctions.showMenu();
                        break;

                    case 3:
                        cafeFunctions.giveOrder();
                        break;

                    case 4:
                        cafeFunctions.detailsOfUsers();

                    default:
                        System.out.println("Enter a valid choice");
                }

        }
    }
}
