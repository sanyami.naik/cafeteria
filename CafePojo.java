package Code2;

public class CafePojo {

    String customerName;
    int noOfSeats;
    int tableNo;
    int totalPrice=0;

    @Override
    public String toString() {
        return "CafePojo{" +
                "customerName='" + customerName + '\'' +
                ", noOfSeats=" + noOfSeats +
                ", tableNo=" + tableNo +
                ", s1=" + s1 +
                ", s2=" + s2 +
                ", s3=" + s3 +
                ", coffeNo=" + coffeNo +
                ", teaNo=" + teaNo +
                ", juiceNo=" + juiceNo +
                ", totalPrice=" + totalPrice +
                '}';
    }

    int s1;
    int s2;
    int s3;

    int coffeNo;
    int teaNo;
    int juiceNo;


    public int getTableNo() {
        return tableNo;
    }

    public void setTableNo(int tableNo) {
        this.tableNo = tableNo;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getNoOfSeats() {
        return noOfSeats;
    }

    public void setNoOfSeats(int noOfSeats) {
        this.noOfSeats = noOfSeats;
    }
}
