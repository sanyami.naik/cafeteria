package Code2;

public class InValidOrderException extends Throwable {

    public InValidOrderException() {
        System.out.println("Atleast one coffee must be ordered while placing an order");
    }
}
