package Code2;

public class NoReservationFoundException extends Throwable {

    public NoReservationFoundException() {
        System.out.println("The table must be reserved before placing an order");
    }
}
