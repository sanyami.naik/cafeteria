package Code2;

import java.util.Comparator;

public class SortByDescendingValues implements Comparator<CafePojo> {

    @Override
    public int compare(CafePojo o1, CafePojo o2) {
        return o2.totalPrice-o1.totalPrice;
    }
}
